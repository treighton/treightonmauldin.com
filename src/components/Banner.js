import React from 'react'

const Banner = props => (
  <section
    id="banner"
    className="major"
    style={{
      backgroundImage: props.backgroundImage,
    }}
  >
    <div className="inner">
      <header className="major">
        <h1>Tr ⑧ on</h1>
      </header>
      <div className="content">
        <p>
          I am a hand crafter of bespoke artisan internet experiences, an
          aspiring photographer, pedagogue of fledgling developers and curator
          of oddities
        </p>
        <ul className="actions">
          <li>
            <a href="#one" className="button next scrolly">
              view my work
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>
)

export default Banner
