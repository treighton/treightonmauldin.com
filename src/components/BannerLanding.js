import React from 'react'

const BannerLanding = props => (
  <section
    id="banner"
    className="style2"
    style={{
      backgroundImage: props.backgroundImage,
    }}
  >
    <div className="inner">
      <header className="major">
        <h1>{props.pageTitle}</h1>
      </header>
      <div className="content">{props.children}</div>
    </div>
  </section>
)

export default BannerLanding
