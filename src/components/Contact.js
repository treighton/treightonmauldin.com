import React from 'react'

class Contact extends React.Component {
  constructor() {
    super()
    this.state = {}
  }

  componentDidMount() {
    this.loadData(this.props)
  }

  loadData = data => {
    this.setState({ ...data })
  }

  handleInput = e => {
    const { name, value } = e.target
    // eslint-disable-next-line
    console.log(name, value)
    this.setState({ [name]: value })
  }

  handleSubmit = e => {
    e.preventDefault()
  }
  render() {
    return (
      <section id="contact">
        <div className="inner">
          <section>
            <form method="post" action="#">
              <div className="field half first">
                <label htmlFor="name">Name</label>
                <input
                  value={this.state.name}
                  type="text"
                  name="name"
                  id="name"
                  onChange={this.handleInput}
                />
              </div>
              <div className="field half">
                <label htmlFor="email">Email</label>
                <input
                  value={this.state.email}
                  type="text"
                  name="email"
                  id="email"
                  onChange={this.handleInput}
                />
              </div>
              <div className="field">
                <label htmlFor="message">Message</label>
                <textarea
                  value={this.state.message}
                  name="message"
                  id="message"
                  rows="6"
                  onChange={this.handleInput}
                />
              </div>
              <ul className="actions">
                <li>
                  <input
                    type="submit"
                    value="Send Message"
                    className="special"
                    onClick={this.handleSubmit}
                  />
                </li>
                <li>
                  <input type="reset" value="Clear" />
                </li>
              </ul>
            </form>
          </section>
          <section className="split">
            <header className="major">
              <h2>Insta</h2>
              <div
                className="instaGrid"
                style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                }}
              />
            </header>
          </section>
        </div>
      </section>
    )
  }
}

export default Contact
