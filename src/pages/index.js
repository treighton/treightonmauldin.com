import React from 'react'
import { Link, graphql } from 'gatsby'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import Banner from '../components/Banner'

import pic01 from '../assets/images/pic01.jpg'

class HomeIndex extends React.Component {
  render() {
    const { edges: posts } = this.props.data.allMdx
    return (
      <Layout>
        <Helmet
          title="TrVIIIon Mauldin"
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        />

        <Banner />

        <div id="main">
          <section id="one" className="tiles">
            {posts.map(({ node: post }) => (
              <article
                key={post.id}
                style={{ backgroundImage: `url(${pic01})` }}
              >
                <header className="major">
                  <h3>{post.frontmatter.title}</h3>
                  <p>{post.excerpt}</p>
                </header>
                <Link to={post.fields.slug} className="link primary" />
              </article>
            ))}
          </section>
          <div className="inner">
            <section>
              <header className="major">
                <h2>About</h2>
              </header>
              <p>
                I am a web developer at{' '}
                <a href="https://pagedesigngroup.com/">Page Design Group</a> in
                Sacramento, CA. We build compelling web pressences for our
                clients using a{' '}
                <a href="https://www.gatsbyjs.org/blog/2018-10-04-journey-to-the-content-mesh/">
                  content mesh
                </a>
                , with a focus on accessability and performance. In addition to
                Page Design, I am also an instructor at the UC Davis Coding
                Bootcamp. If you'd like to learn more feel free drop me a line
                at <a href="mailto:treighton@gmail.com">treighton@gmail.com</a>{' '}
                or fill out that fancy form below.
              </p>
            </section>
          </div>
        </div>
      </Layout>
    )
  }
}

export default HomeIndex

export const pageQuery = graphql`
  query blogIndex {
    allMdx {
      edges {
        node {
          id
          excerpt
          frontmatter {
            title
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
HomeIndex.propTypes = {
  data: PropTypes.object,
}
