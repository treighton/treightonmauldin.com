import React from 'react'
import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import BannerLanding from '../components/BannerLanding'

const Landing = () => (
  <Layout>
    <Helmet
      title="TrVIIIon Mauldin - Photos"
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />

    <BannerLanding
      pageTitle="Photography"
      backgroundImage="url(https://res.cloudinary.com/tr8on/image/upload/v1549947068/IMG_20190117_131720_527.jpg)"
    >
      <p>
        I started taking pictures in late portion 2018 when i bought my first
        DSLR (Canon Rebel T6), and fell in love with it...
      </p>
    </BannerLanding>

    <div id="main">
      <section id="one">
        <div className="inner">
          <header className="major">
            <h2>My Journey</h2>
          </header>
          <p>
            It all started with an impulse Prime Now purchase of a Canon Rebel
            T6, and in the time it took for the initial battery charge I was
            already in love with the idea of taking photos. After a month or so
            of carrying the DSLR with me everywhere and snapping pictures of
            everything I saw, I decided to dust off some of the old cameras that
            my partner inherited from her grandmother. It didn't take long to
            burn through a few rolls of film, and after an upgrade to a Nikon
            FE2, I decided that if I'm going to shoot film, I wanted to be in
            control of the whole process and got all the stuff to develop my
            negatives at home. Now here I am a few months in and ready to share
            my progress. Check out the links below to see my digital or film
            photos.
          </p>
        </div>
      </section>
      <section id="two" className="spotlights">
        <section>
          <Link to="/generic" className="image">
            <img
              src="https://res.cloudinary.com/tr8on/image/upload/v1549587040/2449286_2449286-R1-E026.jpg"
              alt=""
            />
          </Link>
          <div className="content">
            <div className="inner">
              <header className="major">
                <h3>Film</h3>
              </header>
              <p>
                Nullam et orci eu lorem consequat tincidunt vivamus et sagittis
                magna sed nunc rhoncus condimentum sem. In efficitur ligula tate
                urna. Maecenas massa sed magna lacinia magna pellentesque lorem
                ipsum dolor. Nullam et orci eu lorem consequat tincidunt.
                Vivamus et sagittis tempus.
              </p>
              <ul className="actions">
                <li>
                  <Link to="/generic" className="button">
                    Learn more
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section>
          <Link to="/generic" className="image">
            <img
              src="https://res.cloudinary.com/tr8on/image/upload/v1549947068/20190210165959_IMG_2258.jpg"
              alt=""
            />
          </Link>
          <div className="content">
            <div className="inner">
              <header className="major">
                <h3>Digital</h3>
              </header>
              <p>
                Nullam et orci eu lorem consequat tincidunt vivamus et sagittis
                magna sed nunc rhoncus condimentum sem. In efficitur ligula tate
                urna. Maecenas massa sed magna lacinia magna pellentesque lorem
                ipsum dolor. Nullam et orci eu lorem consequat tincidunt.
                Vivamus et sagittis tempus.
              </p>
              <ul className="actions">
                <li>
                  <Link to="/generic" className="button">
                    Learn more
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </section>
      </section>
    </div>
  </Layout>
)

export default Landing
