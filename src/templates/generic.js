import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import { graphql } from 'gatsby'
import PropTypes from 'prop-types'
import MDXRenderer from 'gatsby-mdx/mdx-renderer'
import { Image, CloudinaryContext } from 'cloudinary-react'

const Generic = ({ data: { mdx } }) => (
  <Layout>
    <Helmet>
      <title>Generic - Forty by HTML5 UP</title>
      <meta name="description" content="Generic Page" />
    </Helmet>

    <div id="main" className="alt">
      <section id="one">
        <div className="inner">
          <header className="major">
            <h1>{mdx.frontmatter.title}</h1>
            <p>{mdx.frontmatter.date}</p>
          </header>
          <CloudinaryContext cloudName="tr8on">
            <Image publicId={mdx.frontmatter.image} />
          </CloudinaryContext>
          <MDXRenderer>{mdx.code.body}</MDXRenderer>
        </div>
      </section>
    </div>
  </Layout>
)

export default Generic

export const pageQuery = graphql`
  query BlogPostQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      frontmatter {
        title
        image
        date
      }
      code {
        body
      }
    }
  }
`

Generic.propTypes = {
  data: PropTypes.object,
}
